
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;



public class dessiner extends JPanel {
    private String bcode;         
    private int type;         
    private int vP;         
    private int vN;             
    private int s;
    private int vpred;   
    //Le Constructeur
    public dessiner() {
    	 this.bcode = null;
         this.type = -1;
	}
    
   public void dessin (String c,int m ) {
	   this.bcode = c;
           this.type = m;
       
       vP = this.getHeight() / 4;
       vN = 3 * this.getHeight() / 4;
       s = this.getWidth() / bcode.length();
       repaint();
       
	
} 
   public void paintComponent(Graphics g) {
       g.fillRect(0, 0, this.getWidth(), this.getHeight());

       //s'assurer qu'on les parametres requis pour le dessin du signal
       if (bcode != null || type != -1) {
           dessineGrille(g);

           //Codage selon la methode choisie dans la JCombobox indexées de 0 à 4
           switch (type) {
               case 0:
                   //index "0" pour la methode "NRZ"
                   NRZ(bcode, g);
                   break;
               case 1:
                  //index "1" pour la methode "NRZI"
                  NRZI(g);
                   break;
               case 2:
                   //index "2" pour la methode "manchester"
                   manchester(g);
                   break;

               case 3:
                   //index "3" pour la methode "manchesterDiff"
                   manchesterDiff(g);
                   break;
               case 4:
                   //index "4" pour la methode "miller"
                   miller(g);
                   break;
           }
       }
   }
   
   
   private void dessineGrille(Graphics g) {

	  
       Graphics2D g2 = (Graphics2D) g;
       g2.setColor(Color.red);
       g2.drawLine(0, 0, 0,this.getHeight());
       g2.setColor(Color.WHITE);
       Stroke stroke = g2.getStroke();
       float dash1[] = {5.0f};
       g2.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash1, 0.0f));
       for (int i = s; i < this.getWidth(); i = i +s)
           g2.drawLine(i, 0, i, this.getHeight());
       g2.setStroke(stroke);
       g2.setColor(Color.red);
       g2.drawLine(0, this.getHeight() / 2, this.getWidth(), this.getHeight() / 2);
       g2.setColor(Color.LIGHT_GRAY);
        g.drawString("+nV",3 , this.getHeight() / 4);
   	g.drawString("0V", 3,this.getHeight() / 2);
   	g.drawString("-nV", 3, this.getHeight() / 4 + this.getHeight() / 2);

   }
   
   private void NRZ(String chaine, Graphics g) {
	g.setColor(Color.BLACK);
	int indexP = 0;
	int valP = -1;

	for (int i = 0; i < chaine.length(); i++) {
	   if (chaine.charAt(i) == '0') {

              
               if (valP != 0 && valP != -1)
            	  
                   g.drawLine(indexP, vP, indexP, vN);
               valP= 0;
               g.drawLine(indexP, vN, (indexP + s), vN);
               indexP += s;
           } else {
               
               if (valP!= 1 && valP != -1)
                   g.drawLine(indexP, vP, indexP, vN);
               valP = 1;
               g.drawLine(indexP, vP, (indexP + s), vP);
               indexP += s;
           }

       }}

public void NRZI(Graphics g) {
    String c ="" ;
    for (int i = 0; i < bcode.length(); i++) {
        if (bcode.charAt(i) == '0')
            c += "1";
        else
            c += "0";
    }

    NRZ(c, g);
}
private void miller(Graphics g) {

    
    g.setColor(Color.WHITE);
    int ms = s / 2;
    int vpre = 0;
    int pred = -1;
    int indexP= 0;

    for (int i = 0; i < bcode.length(); i++) {

        if (bcode.charAt(i) == '0') {
            if (i == 0) {

                double r = Math.random();
                if (r < 0.5) {
                    g.drawLine(indexP, vP, indexP+ s, vP);
                    vpre = vP;
                } else {

                    //demarrage en vNeg
                    g.drawLine(indexP, vN, indexP + s, vN);
                    vpre = vN;
                }
                indexP += s;
                pred = 0;
            } else {
                if (pred == 0) {
                    if (vpred == vP) {
                        g.drawLine(indexP, vP, indexP, vN);
                        g.drawLine(indexP, vN, indexP + s, vN);

                        vpred = vN;
                    } else {
                        g.drawLine(indexP, vN, indexP, vP);
                        g.drawLine(indexP, vP, indexP + s, vP);

                        vpred = vP;
                    }
                    indexP += s;
                    pred = 0;
                } else {

                    if (vpred == vP) {
                        g.drawLine(indexP, vP, indexP + s, vP);

                        vpred = vP;
                    } else {
                        g.drawLine(indexP, vN, indexP + s, vN);

                        vpred = vN;
                    }

                    indexP += s;
                    pred = 0;
                }
            }

        } else { 
       if (i == 0) {

                double r = Math.random();
                if (r < 0.5) {
             
                    g.drawLine(0, vP, ms, vP);
                    indexP = ms;

                    g.drawLine(indexP, vP, indexP, vN);

                    g.drawLine(indexP, vN, indexP + ms, vN);

                    vpred = vN;
                } else {
                   
                    g.drawLine(0, vN, ms, vN);
                    indexP = ms;

                    g.drawLine(indexP, vN, indexP, vP);
                    g.drawLine(indexP, vP, indexP + ms, vP);

                    vpred = vP;
                }

                indexP += ms;
                pred = 1;
            } else { 

                if (vpred == vP) {

                    g.drawLine(indexP, vP, indexP + ms, vP);
                    indexP += ms;

                    g.drawLine(indexP, vP, indexP, vN);
                    vpred = vN;

                    g.drawLine(indexP, vN, indexP + ms, vN);


                } else {
                    g.drawLine(indexP, vN, indexP + ms, vN);
                    indexP += ms;

                    g.drawLine(indexP, vN, indexP, vP);
                    vpred = vP;

                    g.drawLine(indexP, vP, indexP + ms, vP);

                }
                indexP += ms;
                pred = 1;

            }

        }

    }

}
private void manchester(Graphics g) {
    g.setColor(Color.BLACK);

    
    int midSpaceH = s / 2;
    int indexPosition = 0;
    int pred = -1;
    for (int i = 0; i < bcode.length(); i++) {
        if (bcode.charAt(i) == '0') {
            if (i == 0) {

            
                g.drawLine(0, vN, midSpaceH, vN);
                indexPosition = midSpaceH;
            } else {
                if (pred == 0) {
                    g.drawLine(indexPosition, vP, indexPosition, vN);

                }
                g.drawLine(indexPosition, vN, (indexPosition + midSpaceH), vN);
                indexPosition += midSpaceH;
            }


           
            g.setColor(Color.BLACK);
            g.drawLine(indexPosition, vN, indexPosition, vP);

          
            g.setColor(Color.BLACK);
            g.drawLine(indexPosition, vP, (indexPosition + midSpaceH), vP);

            indexPosition += midSpaceH;
            pred = 0;
        } else {
            if (i == 0) {
                g.drawLine(0, vP, midSpaceH, vP);
                indexPosition = midSpaceH;
            } else {
                if (pred == 1) {
                    g.drawLine(indexPosition, vN, indexPosition, vP);
                }
                g.drawLine(indexPosition, vP, (indexPosition + midSpaceH), vP);
                indexPosition += midSpaceH;

            }
            g.drawLine(indexPosition, vP, indexPosition, vN);
            g.setColor(Color.BLACK);
            g.drawLine(indexPosition, vN, (indexPosition + midSpaceH), vN);

            indexPosition += midSpaceH;

            pred = 1;

        }
    }
}
private void manchesterDiff(Graphics g) {

    
    g.setColor(Color.BLACK);
    int midSpaceH = s / 2;
    int vPred = 0;
    int indexPosition = 0;
    for (int i = 0; i<bcode.length(); i++) {
        if (bcode.charAt(i) == '0') {
        	if (i == 0) {
                double r = Math.random();
                if (r < 0.5) {
                    g.drawLine(0, vN, 0, vP);

                    g.drawLine(0, vP, midSpaceH, vP);
                    indexPosition = midSpaceH;

                    g.drawLine(indexPosition, vP, indexPosition, vN);
                    vPred = vN;

                    g.drawLine(indexPosition, vN, indexPosition + midSpaceH, vN);

                } else {

                    g.drawLine(0, vP, 0, vN);

                    g.drawLine(0, vN, midSpaceH, vN);
                    indexPosition = midSpaceH;

                    g.drawLine(indexPosition, vN, indexPosition, vP);
                    vPred = vP;

                    g.drawLine(indexPosition, vP, indexPosition + midSpaceH, vP);
                }
                indexPosition += midSpaceH;
            } else {
               
                if (vPred == vP) {
                    g.drawLine(indexPosition, vP, indexPosition, vN);

                    g.drawLine(indexPosition, vN, indexPosition + midSpaceH, vN);
                    indexPosition += midSpaceH;

                    g.drawLine(indexPosition, vN, indexPosition, vP);
                    vPred = vP;

                    g.drawLine(indexPosition, vP, indexPosition + midSpaceH, vP);

                } else {
                    g.drawLine(indexPosition, vN, indexPosition, vP);


                    g.drawLine(indexPosition, vP, indexPosition + midSpaceH, vP);
                    indexPosition += midSpaceH;

                    g.drawLine(indexPosition, vP, indexPosition, vN);
                    vPred = vN;

                    g.drawLine(indexPosition, vN, indexPosition + midSpaceH, vN);
                }
                indexPosition += midSpaceH;
            }
        } else {

            if (i == 0) {
                double r = Math.random();

                if (r < 0.5) {

                    g.drawLine(0, vP, midSpaceH, vP);
                    indexPosition = midSpaceH;

                    g.drawLine(indexPosition, vP, indexPosition, vN);
                    vPred = vN;

                    g.drawLine(indexPosition, vN, indexPosition + midSpaceH, vN);

                } else {

                    g.drawLine(0, vN, midSpaceH, vN);
                    indexPosition = midSpaceH;

                    g.drawLine(indexPosition, vN, indexPosition, vP);
                    vPred = vP;

                    g.drawLine(indexPosition, vP, indexPosition + midSpaceH, vP);
                }
                indexPosition += midSpaceH;
            } else {

                
                if (vPred == vP) {
                    g.drawLine(indexPosition, vP, indexPosition + midSpaceH, vP);
                    indexPosition += midSpaceH;

                    g.drawLine(indexPosition, vP, indexPosition, vN);
                    vPred = vN;

                    g.drawLine(indexPosition, vN, indexPosition + midSpaceH, vN);
                } else {

                    g.drawLine(indexPosition, vN, indexPosition + midSpaceH, vN);
                    indexPosition += midSpaceH;

                    g.drawLine(indexPosition, vN, indexPosition, vP);
                    vPred = vP;

                    g.drawLine(indexPosition, vP, indexPosition + midSpaceH, vP);
                }
                indexPosition += midSpaceH;
            }

        }

    }

}







}
