import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class reseauGraphe extends JFrame implements ActionListener {
  	/**Declaration de nos variables */
	private JPanel pn,ps;
	private JLabel l1,l2;
	private JTextField  t1;
	private JComboBox c;
	private JButton b1;
	private dessiner graphe;
	/**Constructeur*/
	public reseauGraphe() {
		
	     setTitle("La conversion en graphe de transmission"); 
	     init();
	     pack();
	     setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	     setSize(800, 800);
	     setLocationRelativeTo(null);
	     setVisible(true);
	
	
	}
	
	
	/**La fonction d'initialisation*/
public void init() {
	/**Le Cote Nord*/
	pn=new JPanel();
	pn.setBackground(Color.WHITE);
	add(pn,BorderLayout.NORTH);
	c = new JComboBox();
   		c.addItem("NRZ");
   		c.addItem("NRZI");
   		c.addItem("Manchester");
   		c.addItem("Manchester differentiel");
   		c.addItem("Miller");
   	c.setFont(new Font("Helvetica", Font.BOLD, 11));
	pn.add(c);
	
	l2=new JLabel("Entrez code en Binaire:");
	l2.setForeground(new Color(0,0,0));
	pn.add(l2);
	t1=new JTextField(10);
	pn.add(t1);
	
	/**Le cote sud*/
	ps=new JPanel();
	ps.setBackground(Color.WHITE);
	add(ps,BorderLayout.SOUTH);
	b1=new JButton("Comfimer"); 
	b1.addActionListener(this);
	ps.add(b1);
	
	/**Appel de la classe dessiner*/
    	graphe=new dessiner();
 	add(graphe,BorderLayout.CENTER);
	
}
/**Le Main*/
public static void main(String[] arg) {
	/**Instantiation de la classe ReseauGraphe*/
	reseauGraphe rg =new reseauGraphe(); 
	
}




public void actionPerformed(ActionEvent e) {
	int test=0;
	/**Si le code contient que des 0 et 1 */
	String codebinaire = t1.getText();
	String cb = codebinaire;
	for (int i=0;i<cb.length();i++) {
		if(cb.charAt(i)!='0' && cb.charAt(i)!='1') {
			test=-1;
		}
		
	}
	/**Si le code entré par l'utilisateur ne contient pas que 1 et 0 il va nous afficher un onglet qui dit de repeter */
	if(test==-1) {
		JOptionPane jp = new JOptionPane();
		jp.showMessageDialog(this, "Try Again");
	}else {
	
	
	
	String  type = (String) c.getSelectedItem();
	switch (type) {
    case "NRZ":
	graphe.dessin(codebinaire, 0);;
	break;
    case "NRZI":
    	graphe.dessin(codebinaire, 1);
    	break;
    case "Manchester":
    	graphe.dessin(codebinaire, 2);
    	break;
    case "Manchester differentiel":
    	graphe.dessin(codebinaire, 3);
    	break;
    case "Miller":
    	graphe.dessin(codebinaire, 4);
    	break;
    	}
    }
}


}

